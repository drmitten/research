class ProductReviewApprovalWorker
  include Sidekiq::Worker

  BANNED_WORDS = %w[fee nee cruul leent]
  BANNED_REGEX = Regexp.new "\\b(#{BANNED_WORDS*'|'})\\b"

  def perform(product_review_id)
    review = ProductReview.find(product_review_id)

    if review.pending_review?
      if review.comments.match(BANNED_REGEX)
        review.rejection_reason = :bad_language
        review.rejected!
      else
        review.approved!
      end

      ProductReviewNotificationWorker.perform_async(product_review_id)
    else
      # review has already been approved or rejected
      # could be a race condition somewhere or a bug
      # would definitely want to log this as a metric to spot trends
    end

  rescue ActiveRecord::RecordNotFound
    # the record may have been deleted
    # nothing more to do really, although this is just the sort of metric one would like to see logged
  end

end
