class ProductReviewNotificationWorker
  include Sidekiq::Worker

  def perform(product_review_id)
    review = ProductReview.find(product_review_id)

    case
    when review.approved?
      ProductReviewMailer.approval(review).deliver_now
    when review.rejected?
      ProductReviewMailer.rejection(review).deliver_now
    end
  rescue ActiveRecord::RecordNotFound
    # the record may have been deleted
    # nothing more to do really, although this is just the sort of metric one would like to see logged
  end
end
