class Product < ApplicationRecord
  self.table_name = :product

  has_many :product_reviews, foreign_key: :productid
end
