class ProductReview < ApplicationRecord
  self.table_name = :productreview

  enum status: [:pending_review, :approved, :rejected]
  enum rejection_reason: [:bad_language]

  belongs_to :product, foreign_key: :productid
end
