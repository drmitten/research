class ApplicationMailer < ActionMailer::Base
  default from: 'notifications@foo.com'
  layout 'mailer'
end
