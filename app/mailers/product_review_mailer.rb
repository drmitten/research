class ProductReviewMailer < ApplicationMailer
  def approval(review)
    @review = review
    mail(to: review.emailaddress, subject: 'Product Review Approved')
  end

  def rejection(review)
    @review = review
    mail(to: review.emailaddress, subject: 'Product Review Rejected')
  end
end
