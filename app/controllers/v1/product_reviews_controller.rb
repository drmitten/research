class V1::ProductReviewsController < V1::BaseController
  def create
    payload = JSON.parse(request.body.read)

    product = Product.find(payload["productid"])

    # Ensure rating is an integer between 1 and 5, defaulting to 3
    payload["rating"] = payload["rating"].to_i
    payload["rating"] = 3 if payload["rating"].zero?
    payload["rating"] = [[payload["rating"], 5].min, 1].max

    review = product.product_reviews.build(reviewername: payload["name"], emailaddress: payload["email"], comments: payload["review"], rating: payload["rating"])

    review.save!

    ProductReviewApprovalWorker.perform_async(review.productreviewid)

    render json: { success: true, reviewID: review.productreviewid }
  rescue JSON::ParserError => e
    render json: { success: false, message: e.message }
  rescue ActiveRecord::RecordNotFound
    render json: { success: false, message: "Invalid ProductId" }
  rescue StandardError => e
    render json: { success: false, message: "Something went wrong" }
  end
end
