require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web, at: '/jobs'

  namespace :v1 do
    resources :product_reviews, only: :create
  end
end
