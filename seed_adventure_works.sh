#!/bin/bash

pushd tmp
  curl https://github.com/Microsoft/sql-server-samples/releases/download/adventureworks/AdventureWorks-oltp-install-script.zip -L -O -J

  git clone https://github.com/lorint/AdventureWorks-for-Postgres.git

  mv Adventureworks-for-Postgres awp

  unzip AdventureWorks-oltp-install-script.zip -d awp
popd

docker-compose run -w /foo/tmp/awp web ruby update_csvs.rb
docker-compose run web bin/rails db:create
docker-compose run -w /awp db psql -h db -U postgres -d Adventureworks -f install.sql
docker-compose run web bin/rails db:migrate
