class AddApprovalFieldsToProductReview < ActiveRecord::Migration[5.2]
  def up
    add_column :productreview, :status, :integer, default: 0
    add_column :productreview, :rejection_reason, :integer, default: nil

    ProductReview.update_all(status: ProductReview.statuses[:approved])
  end

  def down
    remove_column :productreview, :status
    remove_column :productreview, :rejection_reason
  end
end
