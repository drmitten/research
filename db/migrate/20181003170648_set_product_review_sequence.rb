class SetProductReviewSequence < ActiveRecord::Migration[5.2]
  def up
    ActiveRecord::Base.connection.execute("ALTER SEQUENCE production.productreview_productreviewid_seq START with 100 RESTART;")
  end

  def down
    # noop
  end
end
