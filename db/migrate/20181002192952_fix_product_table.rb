class FixProductTable < ActiveRecord::Migration[5.2]
  def change
    rename_column :product, :class, :klass
  end
end
