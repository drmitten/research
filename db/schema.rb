# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_03_170648) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "tablefunc"
  enable_extension "uuid-ossp"

  create_table "billofmaterials", primary_key: "billofmaterialsid", id: :serial, comment: "Primary key for BillOfMaterials records.", comment: "Items required to make bicycles and bicycle subassemblies. It identifies the heirarchical relationship between a parent product and its components.", force: :cascade do |t|
    t.integer "productassemblyid", comment: "Parent product identification number. Foreign key to Product.ProductID."
    t.integer "componentid", null: false, comment: "Component identification number. Foreign key to Product.ProductID."
    t.datetime "startdate", default: -> { "now()" }, null: false, comment: "Date the component started being used in the assembly item."
    t.datetime "enddate", comment: "Date the component stopped being used in the assembly item."
    t.string "unitmeasurecode", limit: 3, null: false, comment: "Standard code identifying the unit of measure for the quantity."
    t.integer "bomlevel", limit: 2, null: false, comment: "Indicates the depth the component is from its parent (AssemblyID)."
    t.decimal "perassemblyqty", precision: 8, scale: 2, default: "1.0", null: false, comment: "Quantity of the component needed to create the assembly."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "culture", primary_key: "cultureid", id: :string, limit: 6, comment: "Primary key for Culture records.", comment: "Lookup table containing the languages in which some AdventureWorks data is stored.", force: :cascade do |t|
    t.string "name", null: false, comment: "Culture description."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "document", primary_key: "documentnode", id: :string, default: "/", comment: "Primary key for Document records.", comment: "Product maintenance documents.", force: :cascade do |t|
    t.string "title", limit: 50, null: false, comment: "Title of the document."
    t.integer "owner", null: false, comment: "Employee who controls the document.  Foreign key to Employee.BusinessEntityID"
    t.boolean "folderflag", default: false, null: false, comment: "0 = This is a folder, 1 = This is a document."
    t.string "filename", limit: 400, null: false, comment: "File name of the document"
    t.string "fileextension", limit: 8, comment: "File extension indicating the document type. For example, .doc or .txt."
    t.string "revision", limit: 5, null: false, comment: "Revision number of the document."
    t.integer "changenumber", default: 0, null: false, comment: "Engineering change approval number."
    t.integer "status", limit: 2, null: false, comment: "1 = Pending approval, 2 = Approved, 3 = Obsolete"
    t.text "documentsummary", comment: "Document abstract."
    t.binary "document", comment: "Complete document."
    t.uuid "rowguid", default: -> { "public.uuid_generate_v1()" }, null: false, comment: "ROWGUIDCOL number uniquely identifying the record. Required for FileStream."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
    t.index ["rowguid"], name: "document_rowguid_key", unique: true
  end

  create_table "illustration", primary_key: "illustrationid", id: :serial, comment: "Primary key for Illustration records.", comment: "Bicycle assembly diagrams.", force: :cascade do |t|
    t.xml "diagram", comment: "Illustrations used in manufacturing instructions. Stored as XML."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "location", primary_key: "locationid", id: :serial, comment: "Primary key for Location records.", comment: "Product inventory and manufacturing locations.", force: :cascade do |t|
    t.string "name", null: false, comment: "Location description."
    t.decimal "costrate", default: "0.0", null: false, comment: "Standard hourly cost of the manufacturing location."
    t.decimal "availability", precision: 8, scale: 2, default: "0.0", null: false, comment: "Work capacity (in hours) of the manufacturing location."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "product", primary_key: "productid", id: :serial, comment: "Primary key for Product records.", comment: "Products sold or used in the manfacturing of sold products.", force: :cascade do |t|
    t.string "name", null: false, comment: "Name of the product."
    t.string "productnumber", limit: 25, null: false, comment: "Unique product identification number."
    t.boolean "makeflag", default: true, null: false, comment: "0 = Product is purchased, 1 = Product is manufactured in-house."
    t.boolean "finishedgoodsflag", default: true, null: false, comment: "0 = Product is not a salable item. 1 = Product is salable."
    t.string "color", limit: 15, comment: "Product color."
    t.integer "safetystocklevel", limit: 2, null: false, comment: "Minimum inventory quantity."
    t.integer "reorderpoint", limit: 2, null: false, comment: "Inventory level that triggers a purchase order or work order."
    t.decimal "standardcost", null: false, comment: "Standard cost of the product."
    t.decimal "listprice", null: false, comment: "Selling price."
    t.string "size", limit: 5, comment: "Product size."
    t.string "sizeunitmeasurecode", limit: 3, comment: "Unit of measure for Size column."
    t.string "weightunitmeasurecode", limit: 3, comment: "Unit of measure for Weight column."
    t.decimal "weight", precision: 8, scale: 2, comment: "Product weight."
    t.integer "daystomanufacture", null: false, comment: "Number of days required to manufacture the product."
    t.string "productline", limit: 2, comment: "R = Road, M = Mountain, T = Touring, S = Standard"
    t.string "klass", limit: 2, comment: "H = High, M = Medium, L = Low"
    t.string "style", limit: 2, comment: "W = Womens, M = Mens, U = Universal"
    t.integer "productsubcategoryid", comment: "Product is a member of this product subcategory. Foreign key to ProductSubCategory.ProductSubCategoryID."
    t.integer "productmodelid", comment: "Product is a member of this product model. Foreign key to ProductModel.ProductModelID."
    t.datetime "sellstartdate", null: false, comment: "Date the product was available for sale."
    t.datetime "sellenddate", comment: "Date the product was no longer available for sale."
    t.datetime "discontinueddate", comment: "Date the product was discontinued."
    t.uuid "rowguid", default: -> { "public.uuid_generate_v1()" }, null: false
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "productcategory", primary_key: "productcategoryid", id: :serial, comment: "Primary key for ProductCategory records.", comment: "High-level product categorization.", force: :cascade do |t|
    t.string "name", null: false, comment: "Category description."
    t.uuid "rowguid", default: -> { "public.uuid_generate_v1()" }, null: false
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "productcosthistory", primary_key: ["productid", "startdate"], comment: "Changes in the cost of a product over time.", force: :cascade do |t|
    t.integer "productid", null: false, comment: "Product identification number. Foreign key to Product.ProductID"
    t.datetime "startdate", null: false, comment: "Product cost start date."
    t.datetime "enddate", comment: "Product cost end date."
    t.decimal "standardcost", null: false, comment: "Standard cost of the product."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "productdescription", primary_key: "productdescriptionid", id: :serial, comment: "Primary key for ProductDescription records.", comment: "Product descriptions in several languages.", force: :cascade do |t|
    t.string "description", limit: 400, null: false, comment: "Description of the product."
    t.uuid "rowguid", default: -> { "public.uuid_generate_v1()" }, null: false
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "productdocument", primary_key: ["productid", "documentnode"], comment: "Cross-reference table mapping products to related product documents.", force: :cascade do |t|
    t.integer "productid", null: false, comment: "Product identification number. Foreign key to Product.ProductID."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
    t.string "documentnode", default: "/", null: false, comment: "Document identification number. Foreign key to Document.DocumentNode."
  end

  create_table "productinventory", primary_key: ["productid", "locationid"], comment: "Product inventory information.", force: :cascade do |t|
    t.integer "productid", null: false, comment: "Product identification number. Foreign key to Product.ProductID."
    t.integer "locationid", limit: 2, null: false, comment: "Inventory location identification number. Foreign key to Location.LocationID."
    t.string "shelf", limit: 10, null: false, comment: "Storage compartment within an inventory location."
    t.integer "bin", limit: 2, null: false, comment: "Storage container on a shelf in an inventory location."
    t.integer "quantity", limit: 2, default: 0, null: false, comment: "Quantity of products in the inventory location."
    t.uuid "rowguid", default: -> { "public.uuid_generate_v1()" }, null: false
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "productlistpricehistory", primary_key: ["productid", "startdate"], comment: "Changes in the list price of a product over time.", force: :cascade do |t|
    t.integer "productid", null: false, comment: "Product identification number. Foreign key to Product.ProductID"
    t.datetime "startdate", null: false, comment: "List price start date."
    t.datetime "enddate", comment: "List price end date"
    t.decimal "listprice", null: false, comment: "Product list price."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "productmodel", primary_key: "productmodelid", id: :serial, comment: "Primary key for ProductModel records.", comment: "Product model classification.", force: :cascade do |t|
    t.string "name", null: false, comment: "Product model description."
    t.xml "catalogdescription", comment: "Detailed product catalog information in xml format."
    t.xml "instructions", comment: "Manufacturing instructions in xml format."
    t.uuid "rowguid", default: -> { "public.uuid_generate_v1()" }, null: false
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "productmodelillustration", primary_key: ["productmodelid", "illustrationid"], comment: "Cross-reference table mapping product models and illustrations.", force: :cascade do |t|
    t.integer "productmodelid", null: false, comment: "Primary key. Foreign key to ProductModel.ProductModelID."
    t.integer "illustrationid", null: false, comment: "Primary key. Foreign key to Illustration.IllustrationID."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "productmodelproductdescriptionculture", primary_key: ["productmodelid", "productdescriptionid", "cultureid"], comment: "Cross-reference table mapping product descriptions and the language the description is written in.", force: :cascade do |t|
    t.integer "productmodelid", null: false, comment: "Primary key. Foreign key to ProductModel.ProductModelID."
    t.integer "productdescriptionid", null: false, comment: "Primary key. Foreign key to ProductDescription.ProductDescriptionID."
    t.string "cultureid", limit: 6, null: false, comment: "Culture identification number. Foreign key to Culture.CultureID."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "productphoto", primary_key: "productphotoid", id: :serial, comment: "Primary key for ProductPhoto records.", comment: "Product images.", force: :cascade do |t|
    t.binary "thumbnailphoto", comment: "Small image of the product."
    t.string "thumbnailphotofilename", limit: 50, comment: "Small image file name."
    t.binary "largephoto", comment: "Large image of the product."
    t.string "largephotofilename", limit: 50, comment: "Large image file name."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "productproductphoto", primary_key: ["productid", "productphotoid"], comment: "Cross-reference table mapping products and product photos.", force: :cascade do |t|
    t.integer "productid", null: false, comment: "Product identification number. Foreign key to Product.ProductID."
    t.integer "productphotoid", null: false, comment: "Product photo identification number. Foreign key to ProductPhoto.ProductPhotoID."
    t.boolean "primary", default: false, null: false, comment: "0 = Photo is not the principal image. 1 = Photo is the principal image."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "productreview", primary_key: "productreviewid", id: :serial, comment: "Primary key for ProductReview records.", comment: "Customer reviews of products they have purchased.", force: :cascade do |t|
    t.integer "productid", null: false, comment: "Product identification number. Foreign key to Product.ProductID."
    t.string "reviewername", null: false, comment: "Name of the reviewer."
    t.datetime "reviewdate", default: -> { "now()" }, null: false, comment: "Date review was submitted."
    t.string "emailaddress", limit: 50, null: false, comment: "Reviewer's e-mail address."
    t.integer "rating", null: false, comment: "Product rating given by the reviewer. Scale is 1 to 5 with 5 as the highest rating."
    t.string "comments", limit: 3850, comment: "Reviewer's comments"
    t.datetime "modifieddate", default: -> { "now()" }, null: false
    t.integer "status", default: 0
    t.integer "rejection_reason"
  end

  create_table "productsubcategory", primary_key: "productsubcategoryid", id: :serial, comment: "Primary key for ProductSubcategory records.", comment: "Product subcategories. See ProductCategory table.", force: :cascade do |t|
    t.integer "productcategoryid", null: false, comment: "Product category identification number. Foreign key to ProductCategory.ProductCategoryID."
    t.string "name", null: false, comment: "Subcategory description."
    t.uuid "rowguid", default: -> { "public.uuid_generate_v1()" }, null: false
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "scrapreason", primary_key: "scrapreasonid", id: :serial, comment: "Primary key for ScrapReason records.", comment: "Manufacturing failure reasons lookup table.", force: :cascade do |t|
    t.string "name", null: false, comment: "Failure description."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "transactionhistory", primary_key: "transactionid", id: :serial, comment: "Primary key for TransactionHistory records.", comment: "Record of each purchase order, sales order, or work order transaction year to date.", force: :cascade do |t|
    t.integer "productid", null: false, comment: "Product identification number. Foreign key to Product.ProductID."
    t.integer "referenceorderid", null: false, comment: "Purchase order, sales order, or work order identification number."
    t.integer "referenceorderlineid", default: 0, null: false, comment: "Line number associated with the purchase order, sales order, or work order."
    t.datetime "transactiondate", default: -> { "now()" }, null: false, comment: "Date and time of the transaction."
    t.string "transactiontype", limit: 1, null: false, comment: "W = WorkOrder, S = SalesOrder, P = PurchaseOrder"
    t.integer "quantity", null: false, comment: "Product quantity."
    t.decimal "actualcost", null: false, comment: "Product cost."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "transactionhistoryarchive", primary_key: "transactionid", id: :integer, comment: "Primary key for TransactionHistoryArchive records.", default: nil, comment: "Transactions for previous years.", force: :cascade do |t|
    t.integer "productid", null: false, comment: "Product identification number. Foreign key to Product.ProductID."
    t.integer "referenceorderid", null: false, comment: "Purchase order, sales order, or work order identification number."
    t.integer "referenceorderlineid", default: 0, null: false, comment: "Line number associated with the purchase order, sales order, or work order."
    t.datetime "transactiondate", default: -> { "now()" }, null: false, comment: "Date and time of the transaction."
    t.string "transactiontype", limit: 1, null: false, comment: "W = Work Order, S = Sales Order, P = Purchase Order"
    t.integer "quantity", null: false, comment: "Product quantity."
    t.decimal "actualcost", null: false, comment: "Product cost."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "unitmeasure", primary_key: "unitmeasurecode", id: :string, limit: 3, comment: "Primary key.", comment: "Unit of measure lookup table.", force: :cascade do |t|
    t.string "name", null: false, comment: "Unit of measure description."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "workorder", primary_key: "workorderid", id: :serial, comment: "Primary key for WorkOrder records.", comment: "Manufacturing work orders.", force: :cascade do |t|
    t.integer "productid", null: false, comment: "Product identification number. Foreign key to Product.ProductID."
    t.integer "orderqty", null: false, comment: "Product quantity to build."
    t.integer "scrappedqty", limit: 2, null: false, comment: "Quantity that failed inspection."
    t.datetime "startdate", null: false, comment: "Work order start date."
    t.datetime "enddate", comment: "Work order end date."
    t.datetime "duedate", null: false, comment: "Work order due date."
    t.integer "scrapreasonid", limit: 2, comment: "Reason for inspection failure."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  create_table "workorderrouting", primary_key: ["workorderid", "productid", "operationsequence"], comment: "Work order details.", force: :cascade do |t|
    t.integer "workorderid", null: false, comment: "Primary key. Foreign key to WorkOrder.WorkOrderID."
    t.integer "productid", null: false, comment: "Primary key. Foreign key to Product.ProductID."
    t.integer "operationsequence", limit: 2, null: false, comment: "Primary key. Indicates the manufacturing process sequence."
    t.integer "locationid", limit: 2, null: false, comment: "Manufacturing location where the part is processed. Foreign key to Location.LocationID."
    t.datetime "scheduledstartdate", null: false, comment: "Planned manufacturing start date."
    t.datetime "scheduledenddate", null: false, comment: "Planned manufacturing end date."
    t.datetime "actualstartdate", comment: "Actual start date."
    t.datetime "actualenddate", comment: "Actual end date."
    t.decimal "actualresourcehrs", precision: 9, scale: 4, comment: "Number of manufacturing hours used."
    t.decimal "plannedcost", null: false, comment: "Estimated manufacturing cost."
    t.decimal "actualcost", comment: "Actual manufacturing cost."
    t.datetime "modifieddate", default: -> { "now()" }, null: false
  end

  add_foreign_key "billofmaterials", "product", column: "componentid", primary_key: "productid", name: "FK_BillOfMaterials_Product_ComponentID"
  add_foreign_key "billofmaterials", "product", column: "productassemblyid", primary_key: "productid", name: "FK_BillOfMaterials_Product_ProductAssemblyID"
  add_foreign_key "billofmaterials", "unitmeasure", column: "unitmeasurecode", primary_key: "unitmeasurecode", name: "FK_BillOfMaterials_UnitMeasure_UnitMeasureCode"
  add_foreign_key "document", "humanresources.employee", column: "owner", primary_key: "businessentityid", name: "FK_Document_Employee_Owner"
  add_foreign_key "product", "productmodel", column: "productmodelid", primary_key: "productmodelid", name: "FK_Product_ProductModel_ProductModelID"
  add_foreign_key "product", "productsubcategory", column: "productsubcategoryid", primary_key: "productsubcategoryid", name: "FK_Product_ProductSubcategory_ProductSubcategoryID"
  add_foreign_key "product", "unitmeasure", column: "sizeunitmeasurecode", primary_key: "unitmeasurecode", name: "FK_Product_UnitMeasure_SizeUnitMeasureCode"
  add_foreign_key "product", "unitmeasure", column: "weightunitmeasurecode", primary_key: "unitmeasurecode", name: "FK_Product_UnitMeasure_WeightUnitMeasureCode"
  add_foreign_key "productcosthistory", "product", column: "productid", primary_key: "productid", name: "FK_ProductCostHistory_Product_ProductID"
  add_foreign_key "productdocument", "document", column: "documentnode", primary_key: "documentnode", name: "FK_ProductDocument_Document_DocumentNode"
  add_foreign_key "productdocument", "product", column: "productid", primary_key: "productid", name: "FK_ProductDocument_Product_ProductID"
  add_foreign_key "productinventory", "location", column: "locationid", primary_key: "locationid", name: "FK_ProductInventory_Location_LocationID"
  add_foreign_key "productinventory", "product", column: "productid", primary_key: "productid", name: "FK_ProductInventory_Product_ProductID"
  add_foreign_key "productlistpricehistory", "product", column: "productid", primary_key: "productid", name: "FK_ProductListPriceHistory_Product_ProductID"
  add_foreign_key "productmodelillustration", "illustration", column: "illustrationid", primary_key: "illustrationid", name: "FK_ProductModelIllustration_Illustration_IllustrationID"
  add_foreign_key "productmodelillustration", "productmodel", column: "productmodelid", primary_key: "productmodelid", name: "FK_ProductModelIllustration_ProductModel_ProductModelID"
  add_foreign_key "productmodelproductdescriptionculture", "culture", column: "cultureid", primary_key: "cultureid", name: "FK_ProductModelProductDescriptionCulture_Culture_CultureID"
  add_foreign_key "productmodelproductdescriptionculture", "productdescription", column: "productdescriptionid", primary_key: "productdescriptionid", name: "FK_ProductModelProductDescriptionCulture_ProductDescription_Pro"
  add_foreign_key "productmodelproductdescriptionculture", "productmodel", column: "productmodelid", primary_key: "productmodelid", name: "FK_ProductModelProductDescriptionCulture_ProductModel_ProductMo"
  add_foreign_key "productproductphoto", "product", column: "productid", primary_key: "productid", name: "FK_ProductProductPhoto_Product_ProductID"
  add_foreign_key "productproductphoto", "productphoto", column: "productphotoid", primary_key: "productphotoid", name: "FK_ProductProductPhoto_ProductPhoto_ProductPhotoID"
  add_foreign_key "productreview", "product", column: "productid", primary_key: "productid", name: "FK_ProductReview_Product_ProductID"
  add_foreign_key "productsubcategory", "productcategory", column: "productcategoryid", primary_key: "productcategoryid", name: "FK_ProductSubcategory_ProductCategory_ProductCategoryID"
  add_foreign_key "transactionhistory", "product", column: "productid", primary_key: "productid", name: "FK_TransactionHistory_Product_ProductID"
  add_foreign_key "workorder", "product", column: "productid", primary_key: "productid", name: "FK_WorkOrder_Product_ProductID"
  add_foreign_key "workorder", "scrapreason", column: "scrapreasonid", primary_key: "scrapreasonid", name: "FK_WorkOrder_ScrapReason_ScrapReasonID"
  add_foreign_key "workorderrouting", "location", column: "locationid", primary_key: "locationid", name: "FK_WorkOrderRouting_Location_LocationID"
  add_foreign_key "workorderrouting", "workorder", column: "workorderid", primary_key: "workorderid", name: "FK_WorkOrderRouting_WorkOrder_WorkOrderID"
end
