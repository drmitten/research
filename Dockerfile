FROM ruby:2.5
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /foo
WORKDIR /foo
COPY Gemfile /foo/Gemfile
COPY Gemfile.lock /foo/Gemfile.lock
RUN bundle install
COPY . /foo
