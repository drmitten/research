# Getting Started

After cloning the project, the database will need to be seeded with the Adventureworks data.  To do that simply run `./seed_adventure_works.sh`

After that, it's just `docker-compose up` to bring the system online.

# API

### Create Product Review
**http://localhost:3000/v1/product_reviews**

The request should be a JSON object with the following properties (**bold** are required):

* **productid** - the id of the product being reviewed
* **name** - the full name of the person reviewing the product
* **email** - the email address of the person reviewing the product
* **review** - the actual review of the product
* rating - numeric rating of the product from 1 to 5 _(defaults to 3)_

Success response:

* **success** - boolean indicating successful submission (true)
* **reviewID** - ID of newly created review that will now go through the approval process

Error response:

* **success** - boolean indicating unsuccessful submission (false)
* **message** - hopefully helpful string indicating cause of failure

# Notifications

In development mode, no actual emails are sent.

`http://localhost:3000/rails/mailers/product_review_mailer/approval` will show the notification for the latest approved review
`http://localhost:3000/rails/mailers/product_review_mailer/rejection` will show the notification for the latest rejected review
