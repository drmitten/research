# Preview all emails at http://localhost:3000/rails/mailers/product_review_mailer
class ProductReviewMailerPreview < ActionMailer::Preview

  def approval
    ProductReviewMailer.approval(ProductReview.approved.last)
  end

  def rejection
    ProductReviewMailer.rejection(ProductReview.rejected.last)
  end

end
